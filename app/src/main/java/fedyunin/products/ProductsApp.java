package fedyunin.products;

import android.app.Application;

import org.greenrobot.greendao.database.Database;

import fedyunin.products.model.DaoMaster;
import fedyunin.products.model.DaoSession;

/**
 * Created by Никита on 03.01.2017.
 */
public class ProductsApp extends Application {
    private DaoSession daoSession;
    @Override
    public void onCreate() {
        super.onCreate();

        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(this,  "products-db");
        Database db =  helper.getWritableDb();
        daoSession = new DaoMaster(db).newSession();
    }

    public DaoSession getDaoSession() {
        return daoSession;
    }

}
