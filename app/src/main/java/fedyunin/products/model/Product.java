package fedyunin.products.model;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;

import java.util.Objects;

/**
 * Created by Никита on 03.01.2017.
 */
@Entity
public class Product {
    @Id
    private Long id;

    private String caption;
    private String description;

    @Generated(hash = 386612231)
    public Product(Long id, String caption, String description) {
        this.id = id;
        this.caption = caption;
        this.description = description;
    }

    @Generated(hash = 1890278724)
    public Product() {
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    @Override
    public boolean equals(Object obj) {
        return obj != null && obj instanceof Product
                && (Objects.equals(this.id, ((Product) obj).getId()));
    }
}
