package fedyunin.products;

import android.app.Activity;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import fedyunin.products.databinding.ProductDetailBinding;
import fedyunin.products.model.DaoSession;
import fedyunin.products.model.Product;
import fedyunin.products.model.ProductDao;

/**
 * A fragment representing a single Product detail screen.
 * This fragment is either contained in a {@link ProductListActivity}
 * in two-pane mode (on tablets) or a {@link ProductDetailActivity}
 * on handsets.
 */
public class ProductDetailFragment extends Fragment {

    public static final String ARG_ITEM_ID = "item_id";

    private Product mItem;
    private EditText mCaption;
    private EditText mDescription;
    private ProductDao productDao;

    public Long getItemId() {
        return mItem.getId();
    }

    public ProductDetailFragment() {
    }

    @Override
    public void onResume() {
        super.onResume();
        mCaption = (EditText) getActivity().findViewById(R.id.product_caption);
        mDescription = (EditText) getActivity().findViewById(R.id.product_details);

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        DaoSession session = ((ProductsApp) getActivity().getApplication()).getDaoSession();
        productDao = session.getProductDao();

        if (getArguments() != null && getArguments().containsKey(ARG_ITEM_ID)) {
            mItem = productDao.load((Long) getArguments().get(ARG_ITEM_ID));
        } else {
            mItem = new Product();
            View saveMenuButton = getActivity().findViewById(R.id.menu_save);
            if (saveMenuButton != null) {
                saveMenuButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        saveItem();
                        ((ProductListActivity) getActivity()).insertItem(mItem);
                    }
                });
            }
        }

        Activity activity = this.getActivity();

        CollapsingToolbarLayout appBarLayout = (CollapsingToolbarLayout) activity.findViewById(R.id.toolbar_layout);
        if (appBarLayout != null) {
            appBarLayout.setTitle(mItem.getCaption());
        }
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ProductDetailBinding binding = DataBindingUtil.inflate(inflater, R.layout.product_detail, container, false);
        binding.setProduct(mItem);
        return binding.getRoot();
    }


    public void saveItem() {

        mItem.setCaption(String.valueOf(mCaption.getText()));
        mItem.setDescription(String.valueOf(mDescription.getText()));
        mItem.setId(productDao.insertOrReplace(mItem));
        InputMethodManager imm = (InputMethodManager) getActivity()
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        if (getView() != null) {
            imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
        }
        Toast.makeText(getContext(), getString(R.string.saveMessage), Toast.LENGTH_LONG).show();
    }
}
