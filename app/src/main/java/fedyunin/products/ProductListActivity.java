package fedyunin.products;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;
import android.widget.Toast;

import java.util.List;

import fedyunin.products.model.Product;
import fedyunin.products.model.ProductDao;
import fedyunin.products.ui.ProductRecyclerViewAdapter;
import fedyunin.products.ui.ProductViewHolder;

/**
 * An activity representing a list of Products. This activity
 * has different presentations for handset and tablet-size devices. On
 * handsets, the activity presents a list of items, which when touched,
 * lead to a {@link ProductDetailActivity} representing
 * item details. On tablets, the activity presents the list of items and
 * item details side-by-side using two vertical panes.
 */
public class ProductListActivity extends AppCompatActivity {

    /**
     * Whether or not the activity is in two-pane mode, i.e. running on a tablet
     * device.
     */
    private boolean mTwoPane;

    private ProductRecyclerViewAdapter adapter;
    private View.OnClickListener nothingToSaveListener;
    private ProductDao productDao;
    private List<Product> mItems;

    public void insertItem(Product item) {
        if (!mItems.contains(item)) {
            mItems.add(mItems.size(), item);
            adapter.notifyItemInserted(mItems.size() - 1);
        } else {
            adapter.notifyItemChanged(mItems.size() - 1);
        }
    }

    public ProductListActivity() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        productDao = ((ProductsApp) getApplication()).getDaoSession().getProductDao();
        mItems = productDao.loadAll();
        setContentView(R.layout.activity_product_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle(getTitle());

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);

        if (findViewById(R.id.product_detail_container) != null) {
            mTwoPane = true;
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            nothingToSaveListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(getApplicationContext(),
                            R.string.nothingToSaveMessage, Toast.LENGTH_SHORT).show();
                }
            };
            findViewById(R.id.menu_save).setOnClickListener(nothingToSaveListener);

            findViewById(R.id.menu_create).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getSupportFragmentManager()
                            .beginTransaction()
                            .replace(R.id.product_detail_container, new ProductDetailFragment())
                            .commit();
                }
            });

        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            fab.setVisibility(View.VISIBLE);
            findViewById(R.id.menu).setVisibility(View.INVISIBLE);
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(getApplicationContext(), ProductDetailActivity.class);
                    startActivity(intent);
                }
            });
        }


        final RecyclerView recyclerView = (RecyclerView) findViewById(R.id.product_list);
        adapter = new ProductRecyclerViewAdapter(mItems, mTwoPane, this);
        ItemTouchHelper mItemTouchHelper = new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0,
                ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(RecyclerView recyclerView,
                                  RecyclerView.ViewHolder viewHolder,
                                  RecyclerView.ViewHolder target) {
                return false;
            }


            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                Product item = ((ProductViewHolder) viewHolder).mItem;
                productDao.delete(item);
                mItems.remove(item);
                adapter.notifyItemRemoved(viewHolder.getAdapterPosition());
                if (mTwoPane) {
                    ProductDetailFragment currentFragment = (ProductDetailFragment) getSupportFragmentManager()
                            .findFragmentById(R.id.product_detail_container);
                    if (item.getId() != null && currentFragment != null
                            && item.getId().equals(currentFragment.getItemId())) {
                        getSupportFragmentManager().beginTransaction().remove(currentFragment).commit();
                        findViewById(R.id.menu_save).setOnClickListener(nothingToSaveListener);
                    }
                }
            }
        });
        mItemTouchHelper.attachToRecyclerView(recyclerView);
        recyclerView.setAdapter(adapter);

    }
}
