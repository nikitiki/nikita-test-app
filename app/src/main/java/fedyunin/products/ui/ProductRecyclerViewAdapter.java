package fedyunin.products.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import fedyunin.products.ProductDetailActivity;
import fedyunin.products.ProductDetailFragment;
import fedyunin.products.R;
import fedyunin.products.model.Product;

/**
 * Created by Никита on 03.01.2017.
 */
public class ProductRecyclerViewAdapter extends RecyclerView.Adapter<ProductViewHolder> {

    private final List<Product> mItems;
    private final boolean mTwoPane;
    private Activity mParentActivity;

    public ProductRecyclerViewAdapter(List<Product> items, boolean mTwoPane, Activity parentActivity) {
        this.mItems = items;
        this.mTwoPane = mTwoPane;
        this.mParentActivity = parentActivity;
    }

    @Override
    public ProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.product_list_content, parent, false);
        return new ProductViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ProductViewHolder holder, final int position) {
        holder.mItem = mItems.get(position);
        holder.mIdView.setText(String.valueOf(mItems.get(position).getId()));
        holder.mContentView.setText(mItems.get(position).getCaption());

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mTwoPane) {
                    Bundle arguments = new Bundle();
                    arguments.putLong(ProductDetailFragment.ARG_ITEM_ID, holder.mItem.getId());
                    final ProductDetailFragment fragment = new ProductDetailFragment();
                    fragment.setArguments(arguments);
                    ((FragmentActivity) mParentActivity).getSupportFragmentManager()
                            .beginTransaction()
                            .replace(R.id.product_detail_container, fragment)
                            .commit();
                    mParentActivity.findViewById(R.id.menu_save).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            fragment.saveItem();
                            notifyItemChanged(holder.getAdapterPosition());
                        }
                    });
                } else {
                    Context context = v.getContext();
                    Intent intent = new Intent(context, ProductDetailActivity.class);
                    intent.putExtra(ProductDetailFragment.ARG_ITEM_ID, holder.mItem.getId());
                    context.startActivity(intent);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }


}
