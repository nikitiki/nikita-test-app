package fedyunin.products.ui;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import fedyunin.products.R;
import fedyunin.products.model.Product;

/**
 * Created by Никита on 03.01.2017.
 */
public class ProductViewHolder extends RecyclerView.ViewHolder {
    public final View mView;
    public final TextView mIdView;
    public final TextView mContentView;
    public Product mItem;

    public ProductViewHolder(View view) {
        super(view);
        mView = view;
        mIdView = (TextView) view.findViewById(R.id.id);
        mContentView = (TextView) view.findViewById(R.id.content);
    }

    @Override
    public String toString() {
        return super.toString() + " '" + mContentView.getText() + "'";
    }
}